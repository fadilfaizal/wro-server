<?php
include "config.php";

if(!isset($_GET["val"])){
    $query = "SELECT state FROM state WHERE id=0";
    $stmt = $dblink->prepare($query);
    $stmt->bind_result($result);
    $stmt->execute();
    $stmt->fetch();
    echo $result;
} else {
    $val = $_GET["val"];
    $query = "UPDATE state SET state=? WHERE id=0";
    $stmt = $dblink->prepare($query);
    $stmt->bind_param("i", $val);
    echo $stmt->execute();
}