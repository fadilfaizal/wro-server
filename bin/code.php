<?php

if(!isset($_GET["code"]))
    die("Code not given");
else
    $code = $_GET["code"];
if(strlen($code) !== 5 or !ctype_alnum($code))
    die("Invalid Code");

