<?php


//// DB Configuration
require_once("config.php");


//// Basic Functions

// Error Reporting
function kill($line = ""){
    if(empty($line))
        $line = debug_backtrace()[0]["line"];
    die("\"Error code: $line\"");
}

// Login w/ Profile Info
function login($user,$pass){

    // Paramterized SQL Query (To prevent SQL Injection)
    global $dblink;
    $query = "SELECT pass, id, credit FROM users WHERE user=?";
    $stmt = $dblink->prepare($query);
    $stmt->bind_param("s", $user);
    $stmt->bind_result($passenc, $id, $credit);
    $stmt->execute();
    $stmt->fetch();

    // Encrypted Password Verification
    if(!password_verify($pass, $passenc))
        return false;
    else {
        $stmt->close();
        $query = "SELECT id,name,credit FROM users WHERE user=?";
        $stmt = $dblink->prepare($query);
        $stmt->bind_param("s",$user);
        $stmt->bind_result($id,$name,$credit);
        $stmt->execute();
        $stmt->fetch();

        // Profile Details
        $profile = [
            "id" => $id,
            "name" => $name,
            "credit" => $credit
        ];
        $stmt->close();
        return $profile;
    }
}

// Strict MySQL
// $mysqli_driver->report_mode = MYSQLI_REPORT_STRICT;
// ini_set('display_errors',1);
// error_reporting(E_ALL);


//// Input Verification

// Initial check
if(empty($_GET))
    kill();

// Function check
if(empty($_GET["f"]))
    kill();
else
    $function = $_GET["f"];

// Username
if(empty($_GET["user"]))
    kill();
else
    $user = $_GET["user"];

// Password
if(empty($_GET["pass"]))
    kill();
else
    $pass = $_GET["pass"];

// User-Password Combination Check
$profile = login($user,$pass);
if(!$profile)
    kill(0);
$id = $profile["id"];


//// Request-based Functions
switch($function){

    // Login
    case "l":
        die(json_encode(["Profile" => $profile]));
        break;

    // Vending Machine
    case "v":
        if(empty($_GET["name"]))
            kill();
        elseif(empty($_GET["pid"]))
            kill();
        elseif(empty($_GET["code"]))
            kill();
        elseif(empty($_GET["price"]))
            kill();
        elseif(empty($_GET["rdm"]))
            kill();
        elseif(empty($_GET["cash"]))
            kill();
        $returned = 0;
        $query = "INSERT INTO items (name,owner,pid,rdm,code,returned) VALUES (?,?,?,?,?,?)";
        $stmt = $dblink->prepare($query);
        $stmt->bind_param("siidsi", $_GET["name"], $profile["id"], $_GET["pid"], $_GET["rdm"], $_GET["code"], $returned);
        $stmt->execute();
        // var_dump($dblink->error);
        // die('"1"');
        $stmt->close();
        if($_GET["cash"] === "false" or $_GET["cash"] === false){
            $query = "UPDATE users SET credit=? WHERE id=$id";
            $stmt = $dblink->prepare($query);
            $credit = $profile["credit"];
            $credit -= $_GET["price"];
            $stmt->bind_param("i", $credit);
            $stmt->execute();
        }
        die('"1"');
        break;

    // Bin
    case "b":
        if(empty($_GET["code"]))
            kill();
        $code = $_GET["code"];
        $query = "SELECT id,name,pid,rdm,returned FROM items WHERE code=?";
        $stmt = $dblink->prepare($query);
        $stmt->bind_param("s",$code);
        // kill();
        $stmt->bind_result($itemid, $itemname, $pid, $rdm, $returned);
        $stmt->execute();
        $stmt->fetch();
        $item = [
            "name" => $itemname,
            "pid" => $pid,
            "price" => $rdm
        ];
        if($itemname === null)
            kill();
        if($returned)
            kill();
        $stmt->close();
        $profile = login($user,$pass);
        $credit = $profile["credit"];
        $query = "UPDATE users SET credit=? WHERE id=$id";
        $stmt = $dblink->prepare($query);
        $credit += $rdm;
        $stmt->bind_param("d", $credit);
        $stmt->execute();
        $stmt->close();

        $query = "UPDATE items SET returned=1 WHERE id=$itemid";
        $stmt = $dblink->prepare($query);
        $stmt->execute();
        $stmt->close();
        die('"1"');
        break;

    // Item
    case "i":
        $item = [];
        if(empty($_GET["code"]))
            kil1l();
        $code = $_GET["code"];
        if(!ctype_digit(str_replace(",","",$code)))
            kill();
        $query = "SELECT id,name,pid,rdm FROM items WHERE code=?";
        $stmt = $dblink->prepare($query);
        $stmt->bind_param("i",$code);
        $stmt->bind_result($item["id"],$item["name"],$item["pid"],$item["rdm"]);
        $stmt->execute();
        $stmt->fetch();
        $stmt->close();
        die(json_encode($item));
        break;

    // Invalid Function
    default:
        kill();
}

?>
